import "./App.css";
import React from "react";
import ProductList from "./components/productList";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const productList = [
      {
        name: "Doce de abóbora",
        price: 0.5,
        discountPercentage: null,
      },
      {
        name: "Salgadinho",
        price: 2.5,
        discountPercentage: 10,
      },
      {
        name: "Refrigerante",
        price: 8.5,
        discountPercentage: 5,
      },
      {
        name: "Maçã",
        price: 0.7,
        discountPercentage: null,
      },
      {
        name: "Feijão",
        price: 2.7,
        discountPercentage: 15,
      },
    ];

    return (
      <div className="App-header">
        <ProductList list={productList}></ProductList>
      </div>
    );
  }
}

export default App;
