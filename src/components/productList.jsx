import React from "react";
import Sale from "./sale";

class ProductList extends React.Component {
  render() {
    const array = this.props.list;
    return (
      <ul>
        {array.map((produto, index) => {
          return (
            <li key={index}>
              {"Produto: " + produto.name + ", "}
              <br></br>
              {" Preço: " + produto.price + ", "}
              {<Sale>{produto.discountPercentage}</Sale>}
              <br></br>
              <br></br>
            </li>
          );
        })}
      </ul>
    );
  }
}

export default ProductList;
