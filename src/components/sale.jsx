import React from "react";

class Sale extends React.Component {
  render() {
    return (
      <div style={{ color: "red" }}>
        {this.props.children != null &&
          "Desconto de: " + this.props.children + "%"}
      </div>
    );
  }
}
export default Sale;
